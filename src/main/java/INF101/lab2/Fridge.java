package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;
    ArrayList<FridgeItem> itemsInFridge = new ArrayList<FridgeItem> ();

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        return itemsInFridge.size ();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if (totalSize() > nItemsInFridge()){
            itemsInFridge.add (item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(itemsInFridge.contains (item)){
            itemsInFridge.remove (item);
        } else{
                throw new NoSuchElementException(); 
        }
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        itemsInFridge.clear();   
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();

        for(FridgeItem item : itemsInFridge){
            if (item.hasExpired()){
                expiredItems.add (item);
            }
        }
        for(FridgeItem item : expiredItems){
            itemsInFridge.remove (item);
        }
        return expiredItems;
    }
    
}